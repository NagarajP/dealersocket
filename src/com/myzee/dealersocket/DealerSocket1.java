/*
 * Problem Statement:
 * A string s consisting of uppercase English letters is given. In one move, we can delete seven letters from S, forming the word 'BALLOON' (one 'B', one 'A', two 'L's, two 'O's and one 'N'
 * ) and leave a shorter word in S. If the remaining letters in the shortened S are sufficient to allow another instance of the word 'BALLOON' to be removed, next move can be done. 
 * What is the maximum number of such moves that we can apply to S 
 */

package com.myzee.dealersocket;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DealerSocket1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int moves = sol2("BAOBBBOLOHRTBVLOIRGFJDYLNNOLOLGBAX");
		System.out.println(moves);
	}

	public static int sol2(String s) {

		Map<Character, Integer> map = new HashMap<>();
		Map<Character, Integer> balloonMap = new HashMap<>();

		for (Character c : "BALLOON".toCharArray()) {
			if (balloonMap.containsKey(c)) {
				balloonMap.put(c, balloonMap.get(c) + 1);
			} else {
				balloonMap.put(c, 1);
			}
		}

		for (int i = 0; i < s.length(); i++) {
			Character c = s.charAt(i);
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);
			} else {
				map.put(c, 1);
			}
		}
		// System.out.println(map);
		int tot = 0;
		Iterator<Character> it = map.keySet().iterator();
		while (it.hasNext()) {
			Character c = (Character) it.next();
			if (balloonMap.containsKey(c)) {
				tot = tot + map.get(c) - balloonMap.get(c);
			} else {
				tot = tot + map.get(c);
			}

		}
		// Optional<Integer> o = map.values().stream().reduce(Integer::sum);
		return (tot / 7) + 1;
	}
}
